import json
from tornado import gen
from tornado.ioloop import IOLoop
from tornado.web import Application, url, RequestHandler
import algorithms
import data_driver
from local_settings import *
import utils

Driver = data_driver.SparseDataLoader(data_db)
csr = Driver.get_csr_array()
cosine = algorithms.OnlySharedCSRCosineSimilarity(csr)
max_neighbour_count = 200


class NeighboursHandler(RequestHandler):
    @gen.coroutine
    def post(self, **kwargs):
        user_data = json.loads(self.request.body)
        user_scores = user_data['scores']

        result = []
        massage = ''
        cross_count = 0
        if user_scores:
            user_csr = Driver.get_avg_user_csr_array(user_scores)
            similarity, cross_count_array = cosine.count(user_csr)
            cross_count = user_data.get('cross_count', 20)

            result_list, count = utils.get_max_values(similarity, cross_count_array, cross_count, max_neighbour_count)

            result, count = utils.make_result(Driver.replacer, result_list, data_db, count)
            if count:
                message = 'Found {} users with {} cross count'.format(count, cross_count)
            else:
                message = 'Not found users with this cross count: {}'.format(cross_count)
        else:
            message = 'You haven\'t got scores'
        self.write({
            'result': result, 'message': message, 'cross_count': cross_count
        })


def main():
    app = Application([url(r"/getNeighbours", NeighboursHandler), ])
    app.listen(2060, address='127.0.0.1')
    print('ready')
    IOLoop.current().start()


if __name__ == '__main__':
    main()
