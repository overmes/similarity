import pickle
from scipy.sparse import csr_matrix
import numpy as np
from utils import DataReplacer


class SparseDataLoader:
    def __init__(self, db):
        self.db = db
        self.users_col = self.db.users
        self.min_len = 20
        self.max_len = 800

    def scores_filter(self, scores):
        filtered_scores = filter(lambda x: x > 0, [x[0] for x in scores.values()])
        avg = float(sum(filtered_scores)) / len(filtered_scores) if filtered_scores else 0
        if 4 < avg < 9 and len(filtered_scores) < 3000:
            return avg
        else:
            return None

    def _make_replace(self):
        self.scores_count = 0
        for doc in self.users_col.find({'len': {'$gte': self.min_len, '$lte': self.max_len}}):
            user_id = doc['_id']
            user_scores = doc['scores']
            if self.scores_filter(user_scores):
                for item_id, (score, score_type, date) in user_scores.items():
                    item_id = int(item_id)
                    if score > 0:
                        self.replacer.add_or_get_user_index(user_id)
                        self.replacer.add_or_get_item_index(item_id)
                        self.scores_count += 1

    def _prepare_lists(self):
        row = np.zeros(self.scores_count, dtype=np.int32)
        col = np.zeros(self.scores_count, dtype=np.int32)
        data = np.zeros(self.scores_count, dtype=np.float16)

        count = 0
        for doc in self.users_col.find({'len': {'$gte': self.min_len, '$lte': self.max_len}}):
            user_id = doc['_id']
            user_scores = doc['scores']
            user_avg = self.scores_filter(user_scores)
            if user_avg:
                for item_id, (score, score_type, date) in user_scores.items():
                    item_id = int(item_id)
                    if score > 0:
                        user_index = self.replacer.get_user_index(user_id)
                        item_index = self.replacer.get_item_index(item_id)
                        row[count] = user_index
                        col[count] = item_index
                        data[count] = score - user_avg
                        count += 1
        user_count = self.replacer.user_counter
        item_count = self.replacer.item_counter
        csr = csr_matrix((data, (row, col)), shape=(user_count, item_count), dtype=np.float16)
        self.save_data_to_fs(row, col, data)
        row = col = data = None
        return csr

    def get_csr_array(self):
        csr_fs = self.load_data_from_fs()
        if csr_fs is None:
            print 'from db'
            self.replacer = DataReplacer()
            self._make_replace()
            csr = self._prepare_lists()
            return csr
        else:
            print 'from fs'
            return csr_fs

    def get_user_csr_array(self, user_scores):

        col_list = []
        data_list = []
        for item_str_id, (value, score_type) in user_scores.items():
            item_id = int(item_str_id)
            item_index = self.replacer.get_item_index(item_id)
            if value > 0 and item_index:
                col_list.append(item_index)
                data_list.append(value)

        row = np.zeros(len(data_list), dtype=np.int32)
        col = np.array(col_list, dtype=np.int32)
        data = np.array(data_list, dtype=np.float16)

        csr = csr_matrix((data, (row, col)), shape=(1, self.replacer.item_counter), dtype=np.float16)

        return csr

    def get_avg_user_csr_array(self, user_scores):
        filtered_scores = filter(lambda x: x > 0, [x[0] for x in user_scores.values()])
        avg = float(sum(filtered_scores)) / len(filtered_scores)

        col_list = []
        data_list = []
        for item_str_id, (value, score_type, date) in user_scores.items():
            item_id = int(item_str_id)
            item_index = self.replacer.get_item_index(item_id)
            if value > 0 and item_index:
                col_list.append(item_index)
                data_list.append(value - avg)

        row = np.zeros(len(data_list), dtype=np.int32)
        col = np.array(col_list, dtype=np.int32)
        data = np.array(data_list, dtype=np.float16)

        csr = csr_matrix((data, (row, col)), shape=(1, self.replacer.item_counter), dtype=np.float16)

        return csr

    def save_data_to_fs(self, row, col, data):
        np.save('data/row', row)
        np.save('data/col', col)
        np.save('data/data', data)
        pickle.dump(self.replacer, open('data/full_replace.dump', 'w'))

    def load_data_from_fs(self):
        try:
            row = np.load('data/row.npy')
            col = np.load('data/col.npy')
            data = np.load('data/data.npy')
            self.replacer = pickle.load(open('data/full_replace.dump'))
            user_count = self.replacer.user_counter
            item_count = self.replacer.item_counter
            csr = csr_matrix((data, (row, col)), shape=(user_count, item_count), dtype=np.float16)
            row = col = data = None
            return csr
        except BaseException as ex:
            print ex.message
            return None

