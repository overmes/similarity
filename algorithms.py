# coding=utf-8
import numpy as np
import math
from scipy.sparse import csr_matrix


class WrongInputData(Exception):
    def __init__(self, msg, get, need):
        self.msg = '%s: %s \nРазрешены: %s' % (msg, get, need)

    def __str__(self):
        return self.msg


class AbstractSimilarity():
    """
    Абстрактный класс для алгоритмов нахождения схожести
    """
    def __init__(self, all_users_scores):
        self._all_users_scores = all_users_scores
        self._allow_data_types = 'all'
        self._allow_matrix_type = 'all'
        self._allow_shape = 'all'

    def count(self, one_user_scores, cross_count=True):
        self.check_shapes(one_user_scores)
        self.check_matrix_type(one_user_scores)
        self.check_matrix_data_type(one_user_scores)
        if cross_count:
            return self._get_similarity_matrix(one_user_scores), self._get_cross_count_matrix(one_user_scores)
        else:
            return self._get_similarity_matrix(one_user_scores)

    def _get_similarity_matrix(self, one_user_scores):
        pass

    def _get_cross_count_matrix(self, one_user_scores):
        pass

    def check_matrix_type(self, obj):
        if self._allow_matrix_type != 'all' and obj.__class__ != self._allow_matrix_type:
            raise WrongInputData('Не верный тип матрицы', str(obj.__class__), str(self._allow_matrix_type))

    def check_shapes(self, user_scores):
        if self._allow_shape != 'all' and self._allow_shape != user_scores.shape:
            raise WrongInputData('Размерности матриц не совпадают', self._allow_shape, user_scores.shape)

    def check_matrix_data_type(self, obj):
        str_type = obj.dtype.char
        if self._allow_data_types != 'all' and str_type not in self._allow_data_types:
            raise WrongInputData('Не верный тип матрицы', str_type, self._allow_data_types)


class CSRCosineSimilarity(AbstractSimilarity):
    def __init__(self, all_users_scores):
        AbstractSimilarity.__init__(self, all_users_scores)
        self._allow_shape = (1, all_users_scores.shape[1])

        # Разрешены только csr матрицы
        self._allow_matrix_type = csr_matrix
        self.check_matrix_type(all_users_scores)

        # Разрешены float 16, 32, 64, int нельзя, тк возможно деление на ноль и переполнение буфера
        self._allow_data_types = ['e', 'f', 'd']
        self.check_matrix_data_type(all_users_scores)

        # Кэш
        self._A_denom = self._count_A_denom()
        self._A_ones = self._get_ones_A()

    def _count_A_denom(self):
        # Подготавливаем выходной массив
        A_denom = self._all_users_scores.copy()
        # Возводим все оценки в квадрат
        A_denom.data = A_denom.data * A_denom.data

        # Вычисляем сумму квадратов оценок
        A_denom = A_denom.sum(axis=1)

        # Берем корень суммы квадратов оценок
        A_denom = np.sqrt(A_denom)
        return A_denom

    def _get_ones_A(self):
        tempA = self._all_users_scores.copy()
        tempA.data = np.ones(tempA.data.size)
        return tempA

    def _get_similarity_matrix(self, one_user_scores):
        """Функция для вычисления косинусной метрики
        :param one_user_scores: Оценки одного пользователя, csr матрица, float16, минимум одна оценка
        :return: возвращает матрицу столбец результатов сравнения пользователей
        """
        # Вычисляем числитель
        numerator = (self._all_users_scores*one_user_scores.transpose()).todense()

        # Находим сумму квадратов оценок одного пользователя
        B_denom_square_sum = (one_user_scores.data * one_user_scores.data).sum()
        B_denom = math.sqrt(B_denom_square_sum)

        # Вычисляем полный знаминатель, _A_denom расчитывается изначально, тк всегда одинаков
        denominator = self._A_denom * B_denom

        # Желательна проверка на нулевые оценки, иначе будет nan
        # denominator[denominator == 0] = 10000
        res = numerator/denominator

        return res

    def _get_cross_count_matrix(self, one_user_scores):
        """Функция для вычисления количества пересечений для csr матриц
        :param one_user_scores: оценки одного пользователя, csr
        :return: numpy матрица столбец
        """
        B_ones = one_user_scores.copy()
        B_ones.data = np.ones(B_ones.data.size)

        count = (self._A_ones*B_ones.transpose()).todense()
        return count


class OnlySharedCSRCosineSimilarity(AbstractSimilarity):
    def __init__(self, all_users_scores):
        AbstractSimilarity.__init__(self, all_users_scores)
        self._allow_shape = (1, all_users_scores.shape[1])

        # Разрешены только csr матрицы
        self._allow_matrix_type = csr_matrix
        self.check_matrix_type(all_users_scores)

        # Разрешены float 16, 32, 64, int нельзя, тк возможно деление на ноль и переполнение буфера
        self._allow_data_types = ['e', 'f', 'd']
        self.check_matrix_data_type(all_users_scores)

        # Кэш
        self._A_ones = self._get_ones_A()

    def _get_ones_A(self):
        tempA = self._all_users_scores.copy()
        tempA.data = np.ones(tempA.data.size, dtype=np.float32)
        return tempA

    def _get_similarity_matrix(self, one_user_scores):
        """Функция для вычисления косинусной метрики
        :param one_user_scores: Оценки одного пользователя, csr матрица, float16, минимум одна оценка
        :return: возвращает матрицу столбец результатов сравнения пользователей
        """
        # Вычисляем все числители
        numerator = (self._all_users_scores*one_user_scores.transpose()).todense()

        # Находим корень суммы квадратов оценок одного пользователя
        B_denom = one_user_scores.copy()
        B_denom.data = one_user_scores.data * one_user_scores.data
        B_denom = (self._A_ones*B_denom.transpose()).todense()
        B_denom = np.sqrt(B_denom)

        # Находим все корни суммы квадратов других пользователей
        # Подготавливаем фильтр пересечений
        scores_filter = one_user_scores.copy()
        scores_filter.data[:] = np.ones(one_user_scores.data.size, dtype=np.float32)
        # Подготавливаем выходной массив
        A_denom = self._all_users_scores.copy()
        # Возводим все оценки в квадрат
        A_denom.data = A_denom.data * A_denom.data

        # Вычисляем сумму квадратов оценок и фильтруем оценки
        A_denom = (A_denom * scores_filter.transpose()).todense()

        # Берем корень суммы квадратов оценок
        A_denom = np.sqrt(A_denom)

        # Вычисляем полный знаминатель
        denominator = np.multiply(A_denom, B_denom)

        # Желательна проверка на нулевые оценки, иначе будет nan
        denominator[denominator == 0] = 10000
        res = numerator/denominator

        return res

    def _get_cross_count_matrix(self, one_user_scores):
        """Функция для вычисления количества пересечений для csr матриц
        :param one_user_scores: оценки одного пользователя, csr
        :return: numpy матрица столбец
        """
        B_ones = one_user_scores.copy()
        B_ones.data = np.ones(B_ones.data.size)

        count = (self._A_ones*B_ones.transpose()).todense()
        return count


class NumpyCosineSimilarity(AbstractSimilarity):
    def _get_cross_count_matrix(self, one_user_scores):
        """Вычисление количества пересечений для numpy матриц
        :param one_user_scores: оценки одного пользователя
        :return: результирующая матрица столбец
        """
        tempA = self._all_users_scores.copy()
        tempA[np.nonzero(tempA)] = 1

        tempB = one_user_scores.copy()
        tempB[np.nonzero(tempB)] = 1

        count = np.dot(tempA, tempB.transpose())
        return count

    def _get_similarity_matrix(self, one_user_scores):
        """Вычисление косинусной метрики для numpy матриц
        :param one_user_scores: оценки одного пользователя
        :return: numpy результирующая матрица столбец
        """
        numerator = np.dot(self._all_users_scores, one_user_scores.transpose())

        A_denom = np.sqrt(np.power(self._all_users_scores, 2).sum(axis=1))
        B_denom = np.sqrt(np.power(one_user_scores, 2).sum())
        denominator = A_denom * B_denom

        res = numerator/denominator
        return res